﻿using SnowSoftwareSolutions.SnowPortalAPI.Infrastructure.Messaging;
using SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging.Communications.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnowSoftwareSolutions.SnowPortalAPI.IServices
{
    public interface IUserService
    {
        ContractResponse<UserGetListResponse> GetUsers(ContractRequest<UserGetRequest> request);
        ContractResponse<UserGetResponse> GetUser(ContractRequest<UserGetRequest> request);
        ContractResponse<UserGetResponse> AddUser(ContractRequest<UserAddRequest> request);
        ContractResponse<UserGetResponse> UpdateUser(ContractRequest<UserAddRequest> request);
        //ContractResponse<UserGetResponse> GetUserByUserName(ContractRequest<UserGetRequest> request);
    }
}
