﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnowSoftwareSolutions.SnowPortalAPI.IServices
{
    public interface ITimeZoneService
    {
        DateTime Now();
    }
}
