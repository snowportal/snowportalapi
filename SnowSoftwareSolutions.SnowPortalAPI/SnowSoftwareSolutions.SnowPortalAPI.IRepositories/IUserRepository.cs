﻿using SnowSoftwareSolutions.SnowPortalAPI.Infrastructure.Model;
using SnowSoftwareSolutions.SnowPortalAPI.Model.DomainModels;

namespace SnowSoftwareSolutions.SnowPortalAPI.IRepositories
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
