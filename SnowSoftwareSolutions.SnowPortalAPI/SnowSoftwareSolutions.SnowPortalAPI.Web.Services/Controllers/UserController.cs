﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SnowSoftwareSolutions.SnowPortalAPI.Infrastructure.Messaging;
using SnowSoftwareSolutions.SnowPortalAPI.IServices;
using SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging.Communications.User;

namespace SnowSoftwareSolutions.SnowPortalAPI.Web.Services.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/user")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly ILogger<UserController> _logger;

        public UserController(IUserService userService, ILogger<UserController> logger)
        {
            _userService = userService;
            _logger = logger;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var usersResponse = _userService.GetUsers(new ContractRequest<UserGetRequest>());
            return Json(usersResponse);
        }

        [Route("GetById")]
        [HttpGet]
        public JsonResult Get([FromQuery(Name = "id")] Guid id)
        {
            var response = _userService.GetUser(new ContractRequest<UserGetRequest> { Data = new UserGetRequest { Id = id } });
            return Json(response);
        }
        
        //[Route("GetByUserName")]
        //[HttpGet]
        //public JsonResult GetByUserName([FromQuery(Name = "userName")] string userName)
        //{
        //    var response = _userService.GetUserByUserName(new ContractRequest<UserGetRequest> { Data = new UserGetRequest { UserName = userName } });
        //    return Json(response);
        //}

        [HttpPost]
        public IActionResult Add([FromBody] ContractRequest<UserAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _userService.AddUser(request);
            return Json(response);
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromBody] ContractRequest<UserAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _userService.UpdateUser(request);
            return Json(response);
        }
    }
}
