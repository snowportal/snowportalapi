﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using SnowSoftwareSolutions.SnowPortalAPI.Infrastructure.Encryption;
using SnowSoftwareSolutions.SnowPortalAPI.Infrastructure.Messaging;
using SnowSoftwareSolutions.SnowPortalAPI.Infrastructure.UnitOfWork;
using SnowSoftwareSolutions.SnowPortalAPI.IRepositories;
using SnowSoftwareSolutions.SnowPortalAPI.IServices;
using SnowSoftwareSolutions.SnowPortalAPI.Model.DomainModels;
using SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging.Communications.User;
using SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging.Mapping;
using System;
using System.Linq;

namespace SnowSoftwareSolutions.SnowPortalAPI.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILogger<UserService> _logger;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, ILogger<UserService> logger, IUnitOfWork uow, ITimeZoneService timeZoneService, IMapper mapper)

        {
            _userRepository = userRepository;
            _mapper = mapper;
            _logger = logger;
            _timeZoneService = timeZoneService;
            _uow = uow;
        }

        public ContractResponse<UserGetResponse> AddUser(ContractRequest<UserAddRequest> request)
        {
            ContractResponse<UserGetResponse> response;

            try
            {
                var model = request.Data.User.ToUserModel(_mapper);

                var brokenRules = model.GetBrokenRules().ToList();

                if (brokenRules.Any())
                {
                    response = ContractUtil.CreateInvalidResponse<UserGetResponse>(brokenRules);
                }
                else
                {
                    _userRepository.Add(model);
                    _uow.Commit();

                    var responseModel = new UserGetResponse
                    {
                        User = model.ToUserView(_mapper)
                    };

                    response = ContractUtil.CreateResponse(responseModel);
                }


            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<UserGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<UserGetResponse> GetUser(ContractRequest<UserGetRequest> request)
        {
            ContractResponse<UserGetResponse> response;
            try
            {
                var model = _userRepository.First(u => u.Id == request.Data.Id/*, new string[] { nameof(User.UserRole) }*/);
                var modelListResponse = new UserGetResponse
                {
                    User = model.ToUserView(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<UserGetResponse>(ex);
            }

            return response;
        }

        //public ContractResponse<UserGetResponse> GetUserByUserName(ContractRequest<UserGetRequest> request)
        //{
        //    ContractResponse<UserGetResponse> response;
        //    try
        //    {
        //        var model = _userRepository.First(u => u.UserName == request.Data.UserName && u.Active, new string[] { nameof(User.UserRole) });

        //        var modelListResponse = new UserGetResponse
        //        {
        //            User = model.ToUserView(_mapper)
        //        };

        //        response = ContractUtil.CreateResponse(modelListResponse);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(20, ex, ex.Message);
        //        response = ContractUtil.CreateInvalidResponse<UserGetResponse>(ex);
        //    }

        //    return response;
        //}

        public ContractResponse<UserGetListResponse> GetUsers(ContractRequest<UserGetRequest> request)
        {
            ContractResponse<UserGetListResponse> response;
            try
            {
                var models = _userRepository.GetAll();
                var modelListResponse = new UserGetListResponse
                {
                    Users = models.ToUserViewList(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<UserGetListResponse>(ex);
            }

            return response;
        }

        public ContractResponse<UserGetResponse> UpdateUser(ContractRequest<UserAddRequest> request)
        {
            var model = request.Data.User.ToUserModel(_mapper);

            // _userRoleRepository.AddRange(model.UserRole);

            model.UpdateDate = _timeZoneService.Now(); // DateTime.Now;

            var brokenRules = model.GetBrokenRules().ToList();

            if (!brokenRules.Any())
            {
                _userRepository.Edit(model);

                _uow.Commit();

                var responseModel = new UserGetResponse
                {
                    User = model.ToUserView(_mapper)
                };

                return ContractUtil.CreateResponse(responseModel);
            }

            return ContractUtil.CreateInvalidResponse<UserGetResponse>(brokenRules);
        }
    }
}
