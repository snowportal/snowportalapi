﻿using SnowSoftwareSolutions.SnowPortalAPI.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using TimeZoneConverter;

namespace SnowSoftwareSolutions.SnowPortalAPI.Services
{
    public class TimeZoneService : ITimeZoneService
    {
        public DateTime Now()
        {
            DateTime utcTime = DateTime.UtcNow;
            TimeZoneInfo BdZone = TZConvert.GetTimeZoneInfo("Central America Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(utcTime, BdZone);
        }
    }
}
