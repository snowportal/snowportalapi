﻿using SnowSoftwareSolutions.SnowPortalAPI.IRepositories;
using SnowSoftwareSolutions.SnowPortalAPI.Model.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SnowSoftwareSolutions.SnowPortalAPI.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(snowPortalContext context)
            : base(context)
        {
        }
    }
}
