﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging.Communications.User
{
    public class UserGetRequest
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
    }
}
