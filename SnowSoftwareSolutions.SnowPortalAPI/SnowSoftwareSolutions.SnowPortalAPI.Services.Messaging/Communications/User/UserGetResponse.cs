﻿using SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging.ViewModels.User;

namespace SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging.Communications.User
{
    public class UserGetResponse
    {
        public UserView User
        {
            get;
            set;
        }
    }
}
