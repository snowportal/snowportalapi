﻿using SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging.Communications.User
{
    public class UserGetListResponse
    {
        public IEnumerable<UserView> Users { get; set; }
    }
}
