﻿using AutoMapper;
using SnowSoftwareSolutions.SnowPortalAPI.Model.DomainModels;
using SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging.Mapping
{
    public static class UserMapping
    {
        public static IEnumerable<UserView> ToUserViewList(this IEnumerable<User> list, IMapper mapper)
        {
            return mapper.Map<List<UserView>>(list);
        }

        public static UserView ToUserView(this User model, IMapper mapper)
        {
            return mapper.Map<UserView>(model);
        }

        public static User ToUserModel(this UserView modelView, IMapper mapper)
        {
            return mapper.Map<User>(modelView);
        }
    }
}
