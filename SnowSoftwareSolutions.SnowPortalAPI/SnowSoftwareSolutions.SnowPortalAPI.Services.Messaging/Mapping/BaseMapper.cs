﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging.Mapping
{
    public class BaseMapper
    {
        private readonly IMapper _mapper;

        public BaseMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IMapper GetMapper()
        {
            return _mapper;
        }
    }
}
