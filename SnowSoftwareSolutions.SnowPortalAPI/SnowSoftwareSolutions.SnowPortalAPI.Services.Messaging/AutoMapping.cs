﻿using AutoMapper;
using SnowSoftwareSolutions.SnowPortalAPI.Model.DomainModels;
using SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnowSoftwareSolutions.SnowPortalAPI.Services.Messaging
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<User, UserView>().ReverseMap();
        }
    }
}
