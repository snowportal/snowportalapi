﻿using System;
using System.Collections.Generic;

namespace SnowSoftwareSolutions.SnowPortalAPI.Model.DomainModels
{
    public partial class User
    {
        public User()
        {
            UserRole = new HashSet<UserRole>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public bool Active { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Password { get; set; }

        public virtual ICollection<UserRole> UserRole { get; set; }
    }
}
