﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SnowSoftwareSolutions.SnowPortalAPI.Model.DomainModels
{
    public partial class snowPortalContext : DbContext
    {
        public snowPortalContext()
        {
        }

        public snowPortalContext(DbContextOptions<snowPortalContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
