﻿using System;
using System.Collections.Generic;

namespace SnowSoftwareSolutions.SnowPortalAPI.Model.DomainModels
{
    public partial class Role
    {
        public Role()
        {
            UserRole = new HashSet<UserRole>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<UserRole> UserRole { get; set; }
    }
}
