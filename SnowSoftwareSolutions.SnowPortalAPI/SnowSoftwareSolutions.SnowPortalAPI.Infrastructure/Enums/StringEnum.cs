﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace SnowSoftwareSolutions.SnowPortalAPI.Infrastructure.Enums
{
    /// <summary>
    /// This attribute is used to represent a string value
    /// for a value in an enum.
    /// </summary>
    public sealed class StringValueAttribute : Attribute
    {
        public string StringValue { get; private set; }

        public StringValueAttribute(string value)
        {
            StringValue = value;
        }

    }

    /// <summary>
    /// Helper class for working with 'extended' enums using <see cref="StringValueAttribute"/> attributes.
    /// The StringEnum class acts as a wrapper for string value access in enumerations. 
    /// It assumes that enums wishing to expose string values do so via the StringValue attribute
    /// </summary>
    public static class StringEnum
    {
        /// <summary>
        ///  static memory for storing the string values that are being calculated. The idea is to improve performance
        /// when searching for string values in enumerated data types
        /// </summary>
        private static ConcurrentDictionary<string, string> _stringValues = new ConcurrentDictionary<string, string>();

        /// <summary>
        /// Gets from memory a string value already calculated
        /// </summary>
        /// <param name="key">the key to search</param>
        /// <returns>the value of the key</returns>
        public static string GetStringValue(string key)
        {
            var value = string.Empty;
            return _stringValues.TryGetValue(key, out value) ? value : string.Empty;
        }

        /// <summary>
        /// Stores into memory a string value already calculated
        /// </summary>
        /// <param name="key">the key of the string value to store</param>
        /// <param name="value">the value of the string value to store</param>
        /// <returns>true or false</returns>
        public static bool SetStringValue(string key, string value)
        {
            return _stringValues.TryAdd(key, value);
        }

        /// <summary>
        /// Will get the string value for a given enums value, this will
        /// only work if you assign the StringValue attribute to
        /// the items in your enum.
        /// </summary>
        public static string GetStringValue(this Enum value)
        {
            var stringValue = value.ToString();
            var type = value.GetType();
            var key = type.Name + "." + stringValue;

            // look for it in memory first
            var foundValue = GetStringValue(key);
            if (foundValue != string.Empty)
            {
                return foundValue;
            }

            // Get fieldinfo for this type
            var fieldInfo = type.GetField(stringValue);

            // Get the stringvalue attributes
            var attribs = fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];

            // Return the first if there was a match.
            if (attribs != null && attribs.Length > 0)
            {
                foundValue = attribs.First().StringValue;

                // store it in memory
                SetStringValue(key, foundValue);

                return foundValue;
            }

            return null;
        }
    }
}
