﻿using System;
using Microsoft.EntityFrameworkCore;

namespace SnowSoftwareSolutions.SnowPortalAPI.Infrastructure.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        DbContext Context { get; }

        /// <summary>
        /// Saves all pending changes
        /// </summary>
        /// <returns>The number of objects in an Added, Modified, or Deleted state</returns>
        int Commit();
    }
}
