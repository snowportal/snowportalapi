﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnowSoftwareSolutions.SnowPortalAPI.Infrastructure.Messaging
{
    public class BaseRequest
    {       
        public Guid Id { get; set; }
    }
}
